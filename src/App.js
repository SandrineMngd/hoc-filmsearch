import React, { useState } from 'react';
// import logo from './logo.svg';
import './App.css';
import films from './films.json';

function App() {

// variables
const [currentSearch, setCurrentSearch] = useState("")

function searchFilm(event) {
  setCurrentSearch(event.target.value)
}

// Fonction affichage liste des films 
function displayFilms(event) {
  return handleSearch().map((film) => <li key={film.rank}>{film.name}, {film.year}</li>)
}

// Fonction recherche du film dans la liste
function handleSearch() {
  return films.filter(film => film.name.toLowerCase().indexOf(currentSearch.toLowerCase()) !== -1)
}


  return (
    <div className="App">

      <input type="text" placeholder="recherche film" value={currentSearch} onChange={searchFilm}/>

      {/* affichage liste des films */}
      <p>{displayFilms()}</p>



    </div>
  );
}

export default App;
